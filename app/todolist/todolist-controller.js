'use strict';

var TodoCtrl = function InstrutoresCtrl($scope) {

    $scope.lista = [
        { desc: 'Task1', concluido: true, indice: 1 },
        { desc: 'Task2', concluido: false, indice: 2 },
        { desc: 'Task56', concluido: true, indice: 3 }];

    $scope.addTask = function(task){
        var item = { desc: task, concluido: false, indice: $scope.lista.length };
        console.log(item);
        $scope.lista.push(item);

        $("#new_task").val('');
    };

    $scope.removeTask = function(index){
        for(var i = 0; i < $scope.lista.length; i+= 1) {
            if($scope.lista[i].indice === index) {
                console.log(i);
	            $scope.lista.splice(i, 1);
            }
        }
    };

    $scope.updateItem = function(item){
        //$http.get('....', item);
    };

};

angular
    .module('app')
    .controller('TodoCtrl', TodoCtrl);