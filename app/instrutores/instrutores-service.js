'use strict';

var factory = function($http){
    var _getAll = function(success, error){
        return $http.get('http://localhost:3000/api/user').then(success).catch(error);
    };

    return {
        getAll: _getAll
    };
};

angular
    .module('app')
    .factory('InstrutoresAPI', factory);