'use strict';

var instrutoresCtrl = function InstrutoresCtrl($scope, InstrutoresAPI) {

	$scope.loadInstrutores = function(){
        var _success = function(r){ $scope.instrutores = r.data, console.log(r.data) }; 
        var _error = function(r){ alert("ERRO:" + r) };
        InstrutoresAPI.getAll(_success, _error);
    };

    $scope.ShortNameOrEmpty = function(n){
        var arrName = n.split(' ');
        var new_name = 'SEM NOME';
        if(arrName.length >= 1 && n)
	        new_name = arrName[0] + ' ' + arrName[arrName.length - 1].charAt(0) + '.';

        return new_name;
    }
};

angular
    .module('app')
    .controller('InstrutoresCtrl', instrutoresCtrl);