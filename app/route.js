'use strict';

var config = function config($routeProvider){
    $routeProvider

    //Vagas para Instrutores
    .when('/', {
        templateUrl: 'app/template/instrutores.html',
        controller: 'InstrutoresCtrl'
    })

    //TODO LIST
    .when('/todo', {
        templateUrl: 'app/template/todolist.html',
        controller: 'TodoCtrl'
    })

    .otherwise({ redirectTo: '/' });
};

angular
    .module('app')
    .config(config);